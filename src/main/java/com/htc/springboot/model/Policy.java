package com.htc.springboot.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "policy")
public class Policy {
	@Id
	@Column(name = "policy_no")
	private long policyNo;
	private double premium;
	@Temporal(TemporalType.DATE)
	@Column(name = "start_date")
	private Date startDate;
	@Column(name = "policy_mode")
	private String policyMode;

	@ManyToOne
	@JoinColumn(name = "customer_code")
	Customer customer;

	public Policy() {
	}

	public Policy(long policyNo, double premium, Date startDate, String policyMode) {
		super();
		this.policyNo = policyNo;
		this.premium = premium;
		this.startDate = startDate;
		this.policyMode = policyMode;
	}

	public long getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(long policyNo) {
		this.policyNo = policyNo;
	}

	public double getPremium() {
		return premium;
	}

	public void setPremium(double premium) {
		this.premium = premium;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public String getPolicyMode() {
		return policyMode;
	}

	public void setPolicyMode(String policyMode) {
		this.policyMode = policyMode;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	@Override
	public String toString() {
		return "Policy [policyNo=" + policyNo + ", premium=" + premium + ", startDate=" + startDate + ", policyMode="
				+ policyMode + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((policyMode == null) ? 0 : policyMode.hashCode());
		result = prime * result + (int) (policyNo ^ (policyNo >>> 32));
		long temp;
		temp = Double.doubleToLongBits(premium);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Policy other = (Policy) obj;
		if (policyMode == null) {
			if (other.policyMode != null)
				return false;
		} else if (!policyMode.equals(other.policyMode))
			return false;
		if (policyNo != other.policyNo)
			return false;
		if (Double.doubleToLongBits(premium) != Double.doubleToLongBits(other.premium))
			return false;
		if (startDate == null) {
			if (other.startDate != null)
				return false;
		} else if (!startDate.equals(other.startDate))
			return false;
		return true;
	}
}
