package com.htc.springboot.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.htc.springboot.daoservice.InsuranceServiceDAO;
import com.htc.springboot.to.CustomerPolicyTO;
import com.htc.springboot.to.CustomerTO;
import com.htc.springboot.to.PolicyTO;




@Controller
public class InsurancePolicyDAOController {
	
	@Autowired
	InsuranceServiceDAO dao;
	
	@GetMapping("/customerFormDAO")	
	public String showCustomerForm() {
		System.out.println("in add dao");
		return "customer-formdao";
	}
	@PostMapping("/addCustomerDAO")   
	public String addCustomer(@ModelAttribute(name="customer") @Valid CustomerTO customerTO, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
		if(bindingResult.hasErrors()) {
			System.out.println("error");
			return "customer-formdao";
		}
		System.out.println(customerTO);
		boolean result = dao.addCustomer(customerTO);
		if(result) {
			redirectAttributes.addFlashAttribute("customerName", customerTO.getCustomerName());
			redirectAttributes.addFlashAttribute("msg", "Added successfully");
			return "redirect:/addCustomerSuccess";
		}
		else
			return "customer-formdao";
	}
	@GetMapping("/issuePolicyFormDAO")
	public String showIssuePolicyForm() {
		return "policy-formdao";
	}
	@PostMapping(value="/issuePolicyDAO")
	public String issuePolicyToCustomer(@ModelAttribute CustomerPolicyTO customerPolicyTO,  RedirectAttributes attributes) {
		
		System.out.println(customerPolicyTO);
		CustomerTO customerTO = new CustomerTO(customerPolicyTO.getCustomerCode(), customerPolicyTO.getCustomerName(), customerPolicyTO.getAddress(), customerPolicyTO.getContactNo());;
		PolicyTO policyTO = new PolicyTO(customerPolicyTO.getPolicyNo(), customerPolicyTO.getPremium(), customerPolicyTO.getPolicyMode(), customerPolicyTO.getStartDate());;
		
		System.out.println(customerTO);
		System.out.println(policyTO);
		
		boolean result = dao.addCustomerWithPolicy(customerTO, policyTO);
		
		if(result) {
			attributes.addFlashAttribute("policyNo", customerPolicyTO.getPolicyNo());
			attributes.addFlashAttribute("customerName", customerPolicyTO.getCustomerName());
			return "redirect:/issuePolicySuccess";
		}
		else 
			return "policy-form";
	}
	
	@GetMapping("/searchPolicyFormDAO")
	public String searchPolicyForm() {
		return "search-policydao";
	}
	@PostMapping("/getPolicyDetailDAO")
	public ModelAndView getPolicyDetails(@RequestParam(name="policyNo") long policyNo) {
		
		ModelAndView mv = new ModelAndView();
		PolicyTO policyTO = dao.getInsurancePolicy(policyNo);
		if(policyTO == null) {
			mv.setViewName("no-policy");		
			mv.addObject("policyNo", policyNo);
		}
		else {
			mv.setViewName("policy-details");
			mv.addObject("policyTO", policyTO);
		}
		return mv;
	}
	@GetMapping("/searchCustomerFormDAO")
	public String takeNewPolicyForm() {
		return "search-customerdao";
	}
	@PostMapping("/searchCustomerDAO")
	public ModelAndView takeNewPolicy(@RequestParam(name="customerCode") String customerCode) {
		ModelAndView mv = new ModelAndView();
		System.out.println(customerCode);
		CustomerTO customerTO=dao.getCustomerDetails(customerCode);
		System.out.println("before null checking");
		if(customerTO==null) {
			mv.setViewName("no-customer");
			mv.addObject("customerCode",customerCode);
		}
		else {
			mv.setViewName("take-newpolicydao");
			mv.addObject("customerTO",customerTO);
		}
		return mv;
	}
	@PostMapping("/takeNewPolicyDAO")
	public String takeNewPolicyDAO(@ModelAttribute CustomerPolicyTO customerPolicyTO,  RedirectAttributes attributes) {
		//String customerCode=customerPolicyTO.getCustomerCode();
		CustomerTO customerTO = new CustomerTO(customerPolicyTO.getCustomerCode(), customerPolicyTO.getCustomerName(), customerPolicyTO.getAddress(), customerPolicyTO.getContactNo());
		PolicyTO policyTO = new PolicyTO(customerPolicyTO.getPolicyNo(), customerPolicyTO.getPremium(), customerPolicyTO.getPolicyMode(), customerPolicyTO.getStartDate());
		if(dao.takeNewPolicy(customerTO, policyTO)) {
			System.out.println("success");
			attributes.addFlashAttribute("policyNo", customerPolicyTO.getPolicyNo());
			attributes.addFlashAttribute("customerName", customerPolicyTO.getCustomerName());
			return "redirect:/issuePolicySuccess";
		}
		else {
			System.out.println("take-newpolicydao");
			
		}
		return null;		
	}
	@GetMapping("/searchCustomerPolicyDAO")
	public String searchCustomerPolicyDAO() {
		return "search-customerpolicydao";
	}
	
	@PostMapping("/searchCustomerPoliciesDAO")
	public ModelAndView searchCustomerPoliciesDAO(@RequestParam(name="customerCode") String customerCode) {
		ModelAndView mv = new ModelAndView();
		List<PolicyTO> policies = dao.getInsurancePolicies(customerCode);
		System.out.println("in dao");
		if(policies.isEmpty()) {
			mv.setViewName("no-policies");
			mv.addObject("customerCode",customerCode);
		}
		else {
			System.out.println(policies);
			mv.setViewName("customer-policies");
			mv.addObject("policies", policies);
		}
		return mv;
	}
}
