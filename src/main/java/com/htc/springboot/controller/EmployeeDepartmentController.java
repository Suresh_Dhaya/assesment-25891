package com.htc.springboot.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.htc.springboot.model.Employee;

@Controller
public class EmployeeDepartmentController {
	
	@GetMapping("/addEmployeeForm")
	public String addEmployeeForm() {
		return "employee-form";
	}
	@PostMapping("/addEmployee")
	public String addEmployee(@ModelAttribute(name="employee") Employee employee,RedirectAttributes redirectAttributes) {
		System.out.println("in spring addEmployee controller"+employee);
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Employee> requestEntity = new HttpEntity<>(employee, headers);

		ResponseEntity<Boolean> resultEntity = restTemplate.exchange("http://localhost:8080/api/employees",
				HttpMethod.POST,
				requestEntity,
				Boolean.class);
		boolean result = resultEntity.getBody();
		if(result) {
			redirectAttributes.addFlashAttribute("empName",employee.getEmpName());
			redirectAttributes.addFlashAttribute("msg", "Added successfully");
			return "redirect:/addEmployeeSuccess";	//"http://localhost:8080/api/employees",		HttpMethod.POST,		employee,
		}
		else {
			System.out.println("failed");
			return null;
		}
	}
	
	@GetMapping("/addEmployeeSuccess")
	public String addCustomerSuccess() {
		return "employee-addsuccess";
	}
	
	@GetMapping("/searchEmployeeForm")
	public String searchEmployeeForm() {
		return "search-employeeform";
	}
	
	
	@PostMapping("/searchEmployee")
	public ModelAndView getEmployee(@RequestParam(name="empId") int empId) {
		System.out.println("in spring getEmployee controller"+empId);
		RestTemplate restTemplate = new RestTemplate();
		Employee emp = restTemplate.getForObject("http://localhost:8080/api/employees/"+empId, Employee.class);
		System.out.println("in spring controller"+emp);
		ModelAndView mv = new ModelAndView("employee-details","employee",emp);
		return mv;
	}
	@GetMapping("/getAllEmployees")
	public ModelAndView getAllEmployees() {
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<List<Employee>> employeeListEntity = restTemplate.exchange("http://localhost:8080/api/employees",
				HttpMethod.GET,
				entity,
				new ParameterizedTypeReference<List<Employee>>(){});
		List<Employee> empList = employeeListEntity.getBody();
		System.out.println(empList);
		ModelAndView mv = new ModelAndView("list-employees","empList",empList);
		return mv;
	}
	@GetMapping("/searchDepartment")
	public String searchDepartment() {
		return "search-departmentemployees";
	}
	@PostMapping("/searchDepartmentEmployees")
		public ModelAndView getDepartmentEmployees(@RequestParam(name="deptName") String deptName) {
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			HttpEntity<String> entity = new HttpEntity<String>(headers);
			ResponseEntity<List<Employee>> employeeListEntity = restTemplate.exchange("http://localhost:8080/api/employees/departments/"+deptName,
					HttpMethod.GET,
					entity,
					new ParameterizedTypeReference<List<Employee>>(){});
			List<Employee> empList = employeeListEntity.getBody();
			System.out.println(empList);
			ModelAndView mv = new ModelAndView("list-employees","empList",empList);
			return mv;
		}
	@GetMapping("/removeEmployeeForm")
	public String removeEmployeeForm() {
		return "remove-employeeform";
	}
	@PostMapping("/removeEmployee")
	public String removeEmployee(@RequestParam(name="empId") int empId, RedirectAttributes redirectAttributes) {
		RestTemplate restTemplate = new RestTemplate();	
		HttpEntity<Integer> entity = new HttpEntity<Integer>(empId);
		ResponseEntity<Boolean> resultEntity = restTemplate.exchange("http://localhost:8080/api/employees/"+empId,
				HttpMethod.DELETE,
				entity,
				Boolean.class);
		boolean result = resultEntity.getBody();
		if(result) {
			redirectAttributes.addFlashAttribute("empId",empId);
			redirectAttributes.addFlashAttribute("msg", "removed successfully");
			return "redirect:/removeEmployeeSuccess";	//"http://localhost:8080/api/employees",		HttpMethod.POST,		employee,
		}
		else {
			System.out.println("failed");
			return null;
		}	
	}
	@GetMapping("/removeEmployeeSuccess")
	public String removeEmployeeSuccess() {
		return "employee-removesuccess";
	}
}
