package com.htc.springboot.daoservice;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import com.htc.springboot.model.Customer;
import com.htc.springboot.model.Policy;
import com.htc.springboot.to.CustomerTO;
import com.htc.springboot.to.PolicyTO;

@Service
public class InsuranceServiceDAOImple implements InsuranceServiceDAO {
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	private CustomerTO getCustomerTO(Customer customer) {
		CustomerTO customerTO = new CustomerTO();
		customerTO.setCustomerCode(customer.getCustomerCode());
		customerTO.setCustomerName(customer.getCustomerName());
		customerTO.setAddress(customer.getAddress());
		customerTO.setContactNo(customer.getContactNo());
		return customerTO;
	}
	private Customer getCustomer(CustomerTO customerTO) {
		Customer customer= new Customer();
		customer.setCustomerCode(customerTO.getCustomerCode());
		customer.setCustomerName(customerTO.getCustomerName());
		customer.setAddress(customerTO.getAddress());
		customer.setContactNo(customerTO.getContactNo());
		return customer;
	}
	
	private Policy getPolicy(PolicyTO policyTO) {
		Policy policy = new Policy();
		policy.setPolicyNo(policyTO.getPolicyNo());
		policy.setPremium(policyTO.getPremium());
		policy.setPolicyMode(policyTO.getPolicyMode());
		policy.setStartDate(policyTO.getStartDate());
		return policy;
	}
	
	private PolicyTO getPolicyTO(Policy policy) {
		PolicyTO policyTO =new PolicyTO();
		policyTO.setPolicyNo(policy.getPolicyNo());
		policyTO.setPremium(policy.getPremium());
		policyTO.setPolicyMode(policy.getPolicyMode());
		policyTO.setStartDate(policy.getStartDate());
		return policyTO;
	}
	
	@Override
	public boolean addCustomer(CustomerTO customerTO) {
		boolean addFlag = false;
		Customer cust = getCustomer(customerTO);
		int noOfRowsAffected = jdbcTemplate.update("insert into customer values(?,?,?,?)",cust.getCustomerCode(),cust.getCustomerName(),cust.getAddress(),cust.getContactNo());
		if(noOfRowsAffected==1) {
			System.out.println("added using jdbc");
			addFlag=true;
		}
		return addFlag;
	}

	@Override
	public boolean addCustomerWithPolicy(CustomerTO customerTO, PolicyTO policyTO) {
		boolean addFlag=false;
		Customer cust = getCustomer(customerTO);
		Policy policy = getPolicy(policyTO);		
		System.out.println(cust);
		System.out.println(policy);
		int noraCustomer= jdbcTemplate.update("insert into customer values(?,?,?,?)",cust.getCustomerCode(),cust.getCustomerName(),cust.getAddress(),cust.getContactNo());
		int noraPolicy=jdbcTemplate.update("insert into policy values(?,?,?,?,?)",policy.getPolicyNo(),cust.getCustomerCode(),policy.getPremium(),policy.getStartDate(),policy.getPolicyMode());
		if(noraCustomer ==1 && noraPolicy==1)
			addFlag=true;
		return addFlag;
	}

	@Override
	public List<PolicyTO> getInsurancePolicies(String customerCode) {
		List<PolicyTO> policies = new ArrayList<PolicyTO>();
		System.out.println("in dao get insurance policies");
		policies = jdbcTemplate.query("select policy_no,premium,start_date,policy_mode from policy where customer_code=?", new Object[]{customerCode}, 
				new RowMapper<PolicyTO>() {

			@Override
			public PolicyTO mapRow(ResultSet rs, int rowNum) throws SQLException {
				PolicyTO pto = new PolicyTO();
				pto.setPolicyNo(rs.getLong(1));
				pto.setPremium(rs.getDouble(2));
				pto.setStartDate(rs.getDate(3));
				pto.setPolicyMode(rs.getString(4));
				return pto;
				};			
		});
		return policies;
	}

	@Override
	public PolicyTO getInsurancePolicy(long policyNo) {
		PolicyTO policyTO = null;
		System.out.println("in dao get insurance policy");
		NamedParameterJdbcTemplate template = new NamedParameterJdbcTemplate(jdbcTemplate);
		Map<String, Long> paramMap = new HashMap<String, Long>();
		paramMap.put("pno", policyNo);
		policyTO =template.queryForObject("SELECT policy_no,premium,start_date,policy_mode from policy where policy_no=:pno", paramMap,
				new RowMapper<PolicyTO>() {

					@Override
					public PolicyTO mapRow(ResultSet rs, int rowNum) throws SQLException {
						PolicyTO pto = new PolicyTO();
						pto.setPolicyNo(rs.getLong(1));
						pto.setPremium(rs.getDouble(2));
						pto.setStartDate(rs.getDate(3));
						pto.setPolicyMode(rs.getString(4));
						/*
						 * java.sql.Date SqlDate = rs.getDate("start_date"); java.util.Date
						 * SqlDateConverted = new java.util.Date(SqlDate.getTime());
						 * System.out.println(SqlDateConverted); pto.setStartDate(SqlDateConverted);
						 */
						return pto;
					}
			
		});
		return policyTO;
	}

	@Override
	public CustomerTO getCustomerDetails(String customerCode) {
		CustomerTO custTO = null;
		System.out.println("in dao get customer details");
		NamedParameterJdbcTemplate template = new NamedParameterJdbcTemplate(jdbcTemplate);
		Map<String, String> paramMap = new HashMap<String, String>();
		paramMap.put("custCode", customerCode);
		custTO = template.queryForObject("select customer_code,customer_name,address,contact_no from customer where customer_code=:custCode", paramMap,
				new RowMapper<CustomerTO>() {
					@Override
					public CustomerTO mapRow(ResultSet rs, int rowNum) throws SQLException {
						CustomerTO cust=new CustomerTO();
						cust.setCustomerCode(rs.getString(1));
						cust.setCustomerName(rs.getString(2));
						cust.setContactNo(rs.getString(2));
						cust.setContactNo(rs.getString(4));
						return cust;
					}				
				});
		System.out.println("in dao imple get customer details method");
		return custTO;
	}

	@Override
	public boolean takeNewPolicy(CustomerTO customerTO, PolicyTO policyTO) {
		System.out.println("in dao take new policy");
		Customer customer = getCustomer(customerTO);
		Policy policy = getPolicy(policyTO);
		boolean addFlag=false;
		System.out.println(customer);
		System.out.println(policy);
		int noOfRowsAffected = jdbcTemplate.update("insert into policy values(?,?,?,?,?)",policy.getPolicyNo(),customer.getCustomerCode(),policy.getPremium(),policy.getStartDate(),policy.getPolicyMode());
		if(noOfRowsAffected==1) {
			System.out.println("added using jdbc");
			addFlag=true;
		}
		return addFlag;
	}

}
