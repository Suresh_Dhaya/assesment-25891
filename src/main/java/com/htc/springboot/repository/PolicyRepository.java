package com.htc.springboot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.htc.springboot.model.Policy;
import com.htc.springboot.to.PolicyTO;

@Repository
public interface PolicyRepository extends CrudRepository<Policy, Long> {
	/*
	 * @Query("select p from Policy p where Customer.customerCode=:code") public
	 * List<PolicyTO> findByCustomer(@Param("code") String customerCode);
	 */
}
