<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Customer Form</title>
</head>
<body>

<h2> Policy Issue Form</h2>
<form action="issuePolicy" method="post">
	<p><b>Enter customer details</b></p>
	<table>
	<tr><td>Customer code</td> <td><input type="text" name="customerCode"/></td></tr>
	<tr><td>Customer came</td> <td><input type="text" name="customerName"/></td></tr>
	<tr><td>Address</td> <td><input type="text" name="address"/></td></tr>
	<tr><td>Contact number</td> <td><input type="text" name="contactNo"/></td></tr>
	</table>
	
	<p> <b>Enter policy details</b></p>
	<table>
	<tr><td>Policy number</td> <td><input type="text" name="policyNo"/></td></tr>
	<tr><td>Premium</td> <td><input type="text" name="premium"/></td></tr>
	<tr><td>Policy Mode</td> <td><input type="text" name="policyMode"/></td></tr>
	<tr><td>Start Date [DD-MM-YYYY]</td> <td><input type="text" name="startDate"/></td></tr>
	</table> <br/>
	<input type="submit" value="Issue Policy to Customer"/>
	
</form>
</body>
</html>