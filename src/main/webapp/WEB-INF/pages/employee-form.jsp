<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Customer Form</title>
</head>
<body>

<h2> Customer Entry Form</h2>
<form action="addEmployee" method="post">
	<table>
	<tr><td>Employee Id</td> <td><input type="number" name="empId"/></td></tr>
	<tr><td>Employee Name</td> <td><input type="text" name="empName"/></td></tr>
	<tr><td>salary</td> <td><input type="number" name="salary"/></td></tr>
	<tr><td>location</td> <td><input type="text" name="location"/></td></tr>
	<tr><td>Department Name</td> <td><input type="text" name="deptName"/></td></tr>
	<tr><td><input type="submit" value="Save Customer"/></td></tr>
	<tr><td><input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	</table>
</form>
</body>
</html>