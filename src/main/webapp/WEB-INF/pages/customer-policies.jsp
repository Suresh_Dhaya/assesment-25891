<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Customer Policies</title>
</head>
<body>
<h2>Customer Policy Details - Search Results</h2>
<table>
<thead>
			<tr>
				<th>Policy No</th>
				<th>Premium</th>
				<th>Policy Mode</th>
				<th>Start Date</th>
			</tr>
		</thead>
	<tbody>
	<c:forEach var="policyTO" items="${policies}">
	<tr>
	<td> ${policyTO.policyNo}</td>
	<td>${policyTO.premium }</td>
	<td> ${policyTO.policyMode }</td>
	<td>${policyTO.startDate }</td>
	</tr>
	</c:forEach>
	</tbody>
	</table>
</body>
</html>