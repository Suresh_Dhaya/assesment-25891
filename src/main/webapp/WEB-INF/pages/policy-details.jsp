<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Search Policy Details</title>
</head>
<body>

<h2>Policy Details - Search Results</h2>
	<table>
	<tr><td>Policy No</td> <td> ${policyTO.policyNo}</td></tr>
	<tr><td>Premium</td> <td>${policyTO.premium }</td></tr>
	<tr><td>Policy Mode</td> <td> ${policyTO.policyMode }</td></tr>
	<tr><td>Start Date</td> <td>${policyTO.startDate }</td></tr>
	<tr><td><input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	</table>
</body>
</html>