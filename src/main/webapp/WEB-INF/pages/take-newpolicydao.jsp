<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<form action="takeNewPolicyDAO" method="post">
<p> <b>Enter policy details for customer ${customerTO.customerName}</b></p>
	<table>
	<tr><td><input type="hidden" name="customerCode" value="${customerTO.customerCode}"/></td></tr>
	<tr><td><input type="hidden" name="customerName" value="${customerTO.customerName}"/></td></tr>
	<tr><td>Policy number</td> <td><input type="text" name="policyNo"/></td></tr>
	<tr><td>Premium</td> <td><input type="text" name="premium"/></td></tr>
	<tr><td>Policy Mode</td> <td><input type="text" name="policyMode"/></td></tr>
	<tr><td>Start Date [DD-MM-YYYY]</td> <td><input type="text" name="startDate"/></td></tr>
	</table> <br/>
	<input type="submit" value="Take policy"/>
	</form>
</body>
</html>