<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Customer Policies</title>
</head>
<body>
	<h2>Employees Details</h2>
	<table>
		<thead>
			<tr>
				<th>Employee Id</th>
				<th>Employee Name</th>
				<th>Salary</th>
				<th>Location</th>
				<th>Department Name</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="employee" items="${empList}">
				<tr>
					<td>${employee.empId}</td>
					<td>${employee.empName }</td>
					<td>${employee.salary }</td>
					<td>${employee.location }</td>
					<td>${employee.deptName }</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</body>
</html>