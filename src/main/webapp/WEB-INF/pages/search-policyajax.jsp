<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script	src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script	src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script src="static/js/search-policy-ajax.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Customer Form</title>
</head>
<body>

<h2> Search Policy Form</h2>
<form id="get-policy" method="POST">
	<table>
	<tr><td>Enter Policy no</td> <td><input type="text" name="policyNo"/></td><td><span id=policynotexist></span></td></tr>
	<tr> <td colspan="2"><input type="submit" value="Search"/></td></tr>
	<tr><td><input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	</table>
</form>
<form>
	<table>
	<tr><td><input id="policyNo;" type="hidden" name="policyNo"></td></tr>
	<tr><td>Premium</td><td><input id="premium" type="text" name="premium"></td></tr>
	<tr><td>Policy Mode</td><td><input id="policymode" type="text" name="policymode"></td></tr>
	<tr><td>Start Date</td><td><input id="startdate" type="text" name="startdate"></td></tr>
	<tr><td><input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	</table>
</form>
</body>
</html>